﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using System.Data;
using NSABP_BLL;


namespace NSABP.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: Configuration
        [HttpGet]
        public ActionResult Protocol_Form_Config()
        {
            try
            {
                load_dropdowns();
               
            }
            catch (Exception ex)
            {

                
            }
            return View(Configuration_BLL.GetForms());
        }

        //public ActionResult Protocol_Form_Config(IEnumerable<checkboxes_BO> bo, FormCollection form)
        //{
        //    try
        //    {
        //        load_dropdowns();
        //        if (form["forms"] != null)
        //        {
        //            string[] selected_FormIDS = form["forms"].Split(',');
        //            int protocol_Id = Convert.ToInt16(form["Protocol"]);

        //               if( Configuration_BLL.Config_Protocol_form(protocol_Id, selected_FormIDS))
        //            {
        //                ViewBag.Message = "Forms Configured successfully";
        //            }

        //            ViewBag.SelectedItem = protocol_Id;
        //        }
        //        else
        //        {
        //            ViewBag.Message = "Please select at least one form ";
        //        }

        //    }
        //    catch (Exception ex)
        //    {


        //    }
        //    return View(Configuration_BLL.GetForms());

        //}
        [HttpPost]
        public ActionResult Protocol_Form_Config(List<checkboxes_BO> bo)
        {

            try
            {
                if (Configuration_BLL.Config_Protocol_form_Timepoint(bo))
                {


                    return Json(new { Message = "Success" });

                }
                else
                {
                    return Json(new { Message = "Invalid" });
                }
            }
            catch (Exception ex)
            {
            }
            return Json(new { Message = "Invalid" });
        }

        public ActionResult form_chechboxList(int protocol_ID)
        {
            try
            {

                List<int> list_IDs = Configuration_BLL.GetForms_IDS(protocol_ID);
                var list = Configuration_BLL.GetForms().ToList();
                List<checkboxes_BO> res = new List<checkboxes_BO>();
                //changes start TimepointConfiguration
                //foreach (checkboxes_BO bo in list)
                //{
                //    if (list_IDs.Contains(bo.ID))
                //        bo._isChecked = true;
                //    else
                //        bo._isChecked = false;
                //    res.Add(bo);
                //}

                //return PartialView("form_chechboxList", res);

                DataTable dt = Configuration_BLL.GetForms_TimePoints(protocol_ID);
                foreach (checkboxes_BO bo in list)
                {
                    if (dt.Select("T7_FormID =" + bo.ID + "").Count() > 0)
                    {
                        bo._isChecked = true;
                        bo.StrArray = dt.AsEnumerable().Where
                (row => row.Field<int>("T7_FormID") == bo.ID)
                    .Select(s => s.Field<string>("TimePoint"))
                    //.Distinct()
                    .ToList();
                        //    bo.StrArray = dt.Select("T7_FormID = " + bo.ID + "")[0][]

                    }
                    else
                        bo._isChecked = false;
                    res.Add(bo);
                }
                return Json(new { result = res });
            }
            catch (Exception ex)
            {
            }

            return Json(new { result = "Invalid" });

        }


        public void load_dropdowns()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = User_BLL.GetProtocols_Roles_Status();
                DataTable dtprotcol = ds.Tables[0];
              //  ViewBag.Protocol = new SelectList(dtprotcol.AsDataView(), "Id", "Protocol_Code");
                ViewBag.Protocol = new SelectList(User_BLL.GetProtocols().AsDataView(), "Id", "Protocol_Code");
                DataTable dt = ds.Tables[1];
                ViewBag.Role = new SelectList(dt.AsDataView(), "ID", "Role_Name");
              
                //ViewBag.Protocol = new SelectList(User_BLL.GetProtocols().AsDataView(), "Id", "Protocol_Code");

            }
            catch (Exception)
            {

                throw;
            }


        }



    }
}