﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using System.Data;
using NSABP_BLL;

namespace NSABP.Controllers
{
    public class UserController : Controller
    {
    //    public UserController()
    //    {
    //        try
    //        {
    //            var sess = Session["User_ID"];
    //            if (sess == null)
    //            {
    //                RedirectToAction("LoginPage", "UserLogin");

    //            }

    //        }
    //        catch (Exception ex)
    //        {
    //            RedirectToAction("LoginPage", "UserLogin");

    //        }

    //    }
        // GET: User
        public ActionResult GetUserDetails()
        {
            List<User_BO> bolist = NSABP_BLL.User_BLL.GetAllUsers();
            return View(bolist);
        }
        [HttpGet]
        public ActionResult Createuser()
        {
            User_BO obj = new User_BO();
            obj.listSites = Base_BLL.list_Sites();
            Load_Protocol_Role_UStatus();
            return View(obj);
        }
        [HttpPost]
        public ActionResult Createuser(User_BO obj)
        {
            try
            {
                if (!User_BLL.IsUserExists(obj.UserName))
                {
                    if (User_BLL.SaveUserDetails(obj))
                    {
                        //ModelState.Clear();
                        return Json(new { Message = "User details saved successfully" });
                    }
                    else
                        return Json(new { Message = "Invalid operation" });
                }
                else
                {
                    return Json(new { Message = "UserName already exists" });
                }
            }
            catch (Exception)
            {
                return Json(new { Message = "Invalid operation" });
            }

        }
        [HttpGet]
        public ActionResult Edit_User(int ID)
        {
            Load_Protocol_Role_UStatus();
            List<User_BO> list_UserBO = User_BLL.GetAllUsers();
            User_BO bo = list_UserBO.Single(x => x.User_ID == ID);
            bo.List_Protocol_role_site = User_BLL.ListProtocol_role_site(ID);
            ViewBag.sites  = Base_BLL.list_Sites();
            return PartialView("Edit_User", bo);
        }
        [HttpPost]
        public ActionResult Edit_User(User_BO bo)
        {
            Load_Protocol_Role_UStatus();

            if (User_BLL.UpdateUserDetails(bo))
            {
               
                //return View();
                return Json(new { Message = "User details updated successfully" });
            }
            else
            {
                return Json(new { Message = "Invalid operation" });
            }

        }
        public void Load_Protocol_Role_UStatus()
        {
            DataSet ds = new DataSet();
            ds = User_BLL.GetProtocols_Roles_Status();
            DataTable dtprotcol = ds.Tables[0];
            ViewBag.Protocol = new SelectList(dtprotcol.AsDataView(), "Id", "Protocol_Code");
            ViewBag.Protocol1 = dtprotcol.AsDataView();
            DataTable dt = ds.Tables[1];
            //ViewBag.Role = new SelectList(dt.AsDataView(), "ID", "Role_Name");
            ViewBag.Role = new SelectList(dt.Select("Id<>1").CopyToDataTable().AsDataView(), "ID", "Role_Name");
            ViewBag.Role1 = dt.Select("Id<>1").CopyToDataTable().AsDataView();
            DataTable dtStatus = ds.Tables[2];
            ViewBag.Status = new SelectList(dtStatus.AsDataView(), "Status_ID", "Status_Title");
          
            
        }


    }
}