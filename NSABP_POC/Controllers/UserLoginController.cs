﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using NSABP_BLL;
using System.Data;
using System.Web.Security;

namespace NSABP_Product.Controllers
{
    [AllowAnonymous]
    public class UserLoginController : Controller
    {
        [AllowAnonymous]
        public ActionResult LoginPage(string UserName, String PWD)
        {
            LoginBO loginBO = new LoginBO();
            if (UserName != null && PWD != null)
            {
                loginBO.UserName = UserName;
                loginBO.Password = PWD;
                DataTable dt_UserDetails = new DataTable();
                dt_UserDetails = LoginBLL.getuserDetails(loginBO);
                if (dt_UserDetails.Rows.Count > 0)
                {
                    Session["User_ID"] = dt_UserDetails.Rows[0]["User_ID"].ToString();
                    Session["UserName"] = dt_UserDetails.Rows[0]["UserName"].ToString();
                    Session["FName"] = dt_UserDetails.Rows[0]["FName"].ToString();
                    Session["LName"] = dt_UserDetails.Rows[0]["LName"].ToString();
                    Session["PhoneNumber"] = dt_UserDetails.Rows[0]["PhoneNumber"].ToString();
                    Session["email"] = dt_UserDetails.Rows[0]["email"].ToString();
                    List<Protocol_role_site> List_Protocol_role_site = User_BLL.ListProtocol_role_site(Convert.ToInt16(Session["User_ID"]));
                    Session["Role_Name"] = List_Protocol_role_site[0].Role_Name;
                    Session["Protocol_Code"] = List_Protocol_role_site[0].Protocol_Code;
                    Session["Protocol_Id"] = List_Protocol_role_site[0].ProtocolID;
                    Session["Protocol_List"] = List_Protocol_role_site;

                    //authentication
                    FormsAuthentication.SetAuthCookie(dt_UserDetails.Rows[0]["UserName"].ToString(), false);

                    if (List_Protocol_role_site[0].T8_sites_Id != null)
                        Session["Site"] = List_Protocol_role_site[0].T8_sites_Id;
                    if (Session["Role_Name"].ToString() == "SuperAdmin")
                        return RedirectToAction("GetUserDetails", "User");
                    else if (Session["Role_Name"].ToString() == "DIT")
                        return RedirectToAction("FormEntry", "FormEntry");
                    else
                    {
                        List<Forms> List_Forms = Base_BLL.Listforms(Convert.ToInt16(Session["Protocol_Id"]));
                        Session["Forms_List"] = List_Forms;
                        return RedirectToAction(List_Forms[0].URL.Split('/')[2], List_Forms[0].URL.Split('/')[1]);
                    }

                }
                ViewBag.Message = "Login Failed";
                return View();
            }
            else
            {
                return View();
            }
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("LoginPage", "UserLogin");

        }

        public ActionResult Forms_Load(int protocolID)
        {
            List<Protocol_role_site> List_Protocol_role_site = User_BLL.ListProtocol_role_site(Convert.ToInt16(Session["User_ID"]));

            List_Protocol_role_site = List_Protocol_role_site.Where(x => x.ProtocolID == protocolID).ToList();
            Session["Role_Name"] = List_Protocol_role_site[0].Role_Name;
            Session["Protocol_Code"] = List_Protocol_role_site[0].Protocol_Code;
            Session["Protocol_Id"] = List_Protocol_role_site[0].ProtocolID;
            //Session["Protocol_List"] = List_Protocol_role_site;
            if (List_Protocol_role_site[0].T8_sites_Id != null)
                Session["Site"] = List_Protocol_role_site[0].T8_sites_Id;


            if (Session["Role_Name"].ToString() == "SuperAdmin")
                return RedirectToAction("GetUserDetails", "User");
            else if (Session["Role_Name"].ToString() == "DIT")
                return RedirectToAction("FormEntry", "FormEntry");
            else
            {
                List<Forms> List_Forms = Base_BLL.Listforms(Convert.ToInt16(Session["Protocol_Id"]));
                Session["Forms_List"] = List_Forms;
                return RedirectToAction(List_Forms[0].URL.Split('/')[2], List_Forms[0].URL.Split('/')[1]);
            }

        }

        public ActionResult ChangePassword()
        {
            return View();
        }
        public ActionResult ChangePassword(string password)
        {
            if (User_BLL.Changepwd(password, Convert.ToInt16(Session["User_ID"])))
            {

            }
            return View();
        }
    }
}