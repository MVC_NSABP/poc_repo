﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using System.Data;
using NSABP_BLL;

namespace NSABP.Controllers.Forms.FormBLK
{
    public class Form_BLKController : Controller
    {
        public JsonResult getSiteName(string id)
        {
            try
            {
                FormFT_BO bomodel = new FormFT_BO();
                bomodel.listSites = Base_BLL.list_Sites();
                var sitename = bomodel.listSites.Where(x => x.SiteID == id).Select(x => x.Site_Name).FirstOrDefault();

                ViewBag.Patients = new SelectList(Base_BLL.list_Patients(id, "BLK").Where(x => x.Patient_ID.Contains(Session["Protocol_Code"].ToString())), "Patient_ID", "Patient_ID");
                return Json(new { sitename = sitename, Patients = ViewBag.Patients });
            }
            catch (Exception ex)
            {


            }

            return Json(new { invalid = "" });
        }
        // GET: Form_BLK
        [HttpGet]
        //[Authorize(Roles = "SiteUser")]
        public ActionResult FormBLK_Create()
        {           
            ViewBag.formstatus = "create";
            FormBLK_BO bo = new FormBLK_BO();
            bo.FormBLK_PersonCompletingForm_Phone = Session["PhoneNumber"].ToString();
            bo.FormBLK_PersonCompletingForm_Email = Session["email"].ToString();
            bo.FormBLK_PersonCompletingForm_FName = Session["FName"].ToString();
            bo.FormBLK_PersonCompletingForm_LName = Session["LName"].ToString();
            ViewBag.Patients = new SelectList("", "Patient_ID", "Patient_ID");
            //bo.list_Patients = Base_BLL.list_Patients();
            bo.List_Sites = Base_BLL.list_Sites().Where(e => e.SiteID.StartsWith(Session["Site"].ToString().Split('-')[0])).ToList();
            bo.list_Shipping_Company = Base_BLL.list_Shipping_Company();
            return View(bo);
        }
        [HttpPost]
        public JsonResult FormBLK_Create(FormBLK_BO bo)
        {

            //bo.list_Patients = Base_BLL.list_Patients();
            //bo.List_Sites = Base_BLL.list_Sites();
            //bo.list_Shipping_Company = Base_BLL.list_Shipping_Company();

            List<FormBLK_Blocks_BO> list_blks = new List<FormBLK_Blocks_BO>();
            foreach (var item in bo.temp.Split(','))
            {
                FormBLK_Blocks_BO blks_bo = new FormBLK_Blocks_BO();
                blks_bo.Block_Slide_ID = item;
                list_blks.Add(blks_bo);
            }
            bo.List_Blocks = list_blks;
            bo.Created_By = Convert.ToInt16(Session["User_ID"]);

            if (bo.ID == 0)//create
            {
                int i = FormBLK_BLL.SaveFormBLKDetails(bo);
                if (i > 0)
                {
                    bo.ID = i;
                    //ViewBag.Message = "Form Block saved Successfully.";
                    return Json(new { status = "Success", message = "Success" });
                }
            }
            else if (bo.ID > 0)//update
            {
                if (FormBLK_BLL.EditFormBLKDetails(bo))
                {
                    return Json(new { status = "Success", message = "Success" });
                }

            }

            return Json(new { status = "failed", message = "Success" });

        }

        [HttpPost]
        public ActionResult FormBLK_SaveShipmnt(FormBLK_BO bo)
        {
            bo.Created_By = Convert.ToInt16(Session["User_ID"]);
            if (FormBLK_BLL.SaveFormBLKShippemnt(bo))
            {
                //ViewBag.Message = "Form Block Shipped Successfully.";
                return Json(new { status = "Success", message = "Success" });
            }

            return Json(new { status = "Failed", message = "Success" });
        }

        //to grid
        [HttpGet]
        public ActionResult FormBLK_Details()
        {

            ViewBag.formstatus = "edit";
            Protocol_role_site bo = new Protocol_role_site();
            bo.ProtocolID = Convert.ToInt16(Session["Protocol_Id"]);
            bo.Role_Name = Session["Role_Name"].ToString();

            if (Session["Role_Name"].ToString() == "SiteUser")
                bo.T8_sites_Id = Session["Site"].ToString();
            return View(FormBLK_BLL.FormBLKDetails(bo));
        }

        //to popup
        [HttpGet]
        public ActionResult EditFormBLK_Details(int id)
        {
            ViewBag.formstatus = "edit";
            FormBLK_BO bo = new FormBLK_BO();
            bo = FormBLK_BLL.FormBLKDetails(id);
            List<SelectListItem> Patients = new List<SelectListItem>();
            Patients.Add(new SelectListItem() { Text = bo.T1_PatientID, Value = bo.T1_PatientID });
            ViewBag.Patients = new SelectList(Patients, "Value", "Text");
            bo.List_Sites = Base_BLL.list_Sites();
            bo.list_Shipping_Company = Base_BLL.list_Shipping_Company();
            bo.List_Blocks = FormBLK_BLL.BlockSlideDetails(id);
            bo.list_Receieved_Conditions = Base_BLL.list_ReceivedCondition();
            return PartialView("~/Views/Form_BLK/PartialViews/P_FormBLKCreate.cshtml", bo);
        }


        [HttpGet]

        public ActionResult GetFormBLK_DMG_Details()
        {
            Protocol_role_site bo = new Protocol_role_site();
            bo.ProtocolID = Convert.ToInt16(Session["Protocol_Id"]);
            bo.Role_Name = Session["Role_Name"].ToString();

            return View(FormBLK_BLL.FormBLKDetails(bo));
        }
        [HttpGet]
        public PartialViewResult FormBLK_DMG_Receive(int ID)
        {
            FormBLK_BO bo = new FormBLK_BO();

            bo = FormBLK_BLL.FormBLKDetails(ID);
            bo.list_Receieved_Conditions = Base_BLL.list_ReceivedCondition();
            return PartialView("~/Views/Form_BLK/PartialViews/FormBLK_DMG_Receive.cshtml", bo);
        }
        [HttpPost]
        public JsonResult FormBLK_DMG_Receive(FormBLK_BO bo)
        {
            bo.Created_By = bo.DMG_Receieved_By = Convert.ToInt16(Session["User_ID"]);
            if (FormBLK_BLL.ReceiveFormBLKDMG(bo))
            {
                return Json(new { status = "Success", message = "Success" });
            }

            return Json(new { status = "failed", message = "Success" });
        }




    }
}