﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NSABP_Model_BO_;
using System.Data;
using NSABP_BLL;
namespace NSABP.Controllers.Forms
{
    public class Form_BNKController : Controller
    {
        // GET: Form_BNK
        //public ActionResult Index()
        //{
        //    return View();
        //}
        [HttpGet]
        public ActionResult FormBNK_Create()
        {
            ViewBag.formstatus = "create";
            FormBLK_BO bo = new FormBLK_BO();
            bo.FormBLK_PersonCompletingForm_Phone = Session["PhoneNumber"].ToString();
            bo.FormBLK_PersonCompletingForm_Email = Session["email"].ToString();
            bo.FormBLK_PersonCompletingForm_FName = Session["FName"].ToString();
            bo.FormBLK_PersonCompletingForm_LName = Session["LName"].ToString();
            ViewBag.Patients = new SelectList("", "Patient_ID", "Patient_ID");
            //bo.list_Patients = Base_BLL.list_Patients();
            bo.List_Sites = Base_BLL.list_Sites().Where(e => e.SiteID.StartsWith(Session["Site"].ToString().Split('-')[0])).ToList();
            bo.list_Shipping_Company = Base_BLL.list_Shipping_Company();
            return View(bo);
            //FormBNK_BO bo = new FormBNK_BO();
            ////bo.list_Patients = Base_BLL.list_Patients();
            //bo.List_Sites = Base_BLL.list_Sites();
            //bo.FormBNK_PersonCompletingForm_Phone = Session["PhoneNumber"].ToString();
            //bo.FormBNK_PersonCompletingForm_Email = Session["email"].ToString();
            //bo.FormBNK_PersonCompletingForm_FName = Session["FName"].ToString();
            //bo.FormBNK_PersonCompletingForm_LName = Session["LName"].ToString();
            //return View(bo);
        }
        [HttpPost]
        public ActionResult FormBNK_Create(FormBNK_BO bo)
        {
            //bo.list_Patients = Base_BLL.list_Patients();
            //bo.List_Sites = Base_BLL.list_Sites();
            //// bo.list_Shipping_Company = Base_BLL.list_Shipping_Company();
            //var selectedItem = bo.List_Sites.Find(p => p.Site_Name == bo.SiteID.ToString());
            //bo.SiteID = selectedItem.SiteID;
            //bo.Site_Name = selectedItem.Site_Name;
            if (bo.Peripheral_venous == true)
                bo.Peripheral_Venious_Central_Access_Deveice_C1D1 = "PeripheralVenous";
            else if (bo.Peripheral_venous == false)
                bo.Peripheral_Venious_Central_Access_Deveice_C1D1 = "CentralAccess";
            if (ModelState.IsValid)
            {
                if (FormBNK_BLL.SaveBNKDetails(bo))
                {
                    ModelState.Clear();
                    ViewBag.Message = "FormBNK details saved successfully";
                }
                else
                    ViewBag.Message = "Invalid operation";
            }

            return View();

        }
        public ActionResult FormBNK_Details()
        {
            List<FormBNK_BO> bolist = FormBNK_BLL.GetFormBNK_Details("", "");
            return View(bolist);

        }
        [HttpGet]
        public ActionResult FormBNKEdit(int ID)
        {

            FormBNK_BO bo = new FormBNK_BO();
            //bo.list_Patients = Base_BLL.list_Patients();
            bo.List_Sites = Base_BLL.list_Sites();
            return PartialView("FormBNKEdit", bo);


        }
    }
}