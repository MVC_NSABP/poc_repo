﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using NSABP_Model_BO_;
using System.Data;

namespace NSABP_DAL
{
    public class FormFT_DAL
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;


        public static bool SaveFormFTDetails(FormFT_BO bo)
        {
            string strQuery;
            bool isSave = false;
            try
            {
                Conn = new SqlConnection(Co);
                strQuery = "insert into T10_FormFT(Form_FT_ID,T9_PatientID,LiverTumorIssue,OtherIssue,SubMergedIn10PercentFormalinVial,DateBiospecimenCollected,SiteOfOtherTumorIssue,FormFT_PersonCompletingForm_FName,FormFT_PersonCompletingForm_LName,FormFT_PersonCompletingForm_Phone,FormFT_PersonCompletingForm_Email,[Created_By],[Created_On],Record_Status)";
                strQuery += "values('" + bo.Form_FT_ID + "','" + bo.T9_PatientID + "','" + bo.LiverTumorIssue + "','" + bo.OtherTIssue + "','" + bo.SubMergedIn10PercentFormalinVial + "','" + bo.DateBiospecimenCollected + "',";
                if (bo.OtherTIssue)
                {
                    strQuery += "'" + bo.SiteOfOtherTumorTIssue + "',";
                }
                else
                {
                    strQuery += "null,";
                }
                strQuery += "'" + bo.FormFT_PersonCompletingForm_FName + "','" + bo.FormFT_PersonCompletingForm_LName + "','" + bo.FormFT_PersonCompletingForm_Phone + "','" + bo.FormFT_PersonCompletingForm_Email + "','" + bo.Created_By + "',getdate(),'Active')";
                strQuery += ";update T9_Patients set FormFTCreatedDate=getdate() ,Modified_By='" + bo.Modified_By + "',Modified_On=getdate() where [Patient_ID]='" + bo.T9_PatientID + "'";
                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                isSave = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return isSave;
        }

        public static bool SaveShipmentDetails(FormFT_BO bo)
        {
            bool isSave = false;
            SqlConnection Con = new SqlConnection(Co);
            try
            {
                SqlCommand cmd = new SqlCommand("update T10_FormFT set shippingCompanyId=" + bo.shippingCompanyId + ",trackingNumber='" + bo.trackingNumber + "',dateShippedToDivisionOfPathology='" + bo.dateShippedToDivisionOfPathology + "',Modified_By='" + bo.Modified_By + "',Modified_On=getdate() where Form_FT_ID='" + bo.Form_FT_ID + "' ", Con);
                Con.Open();
                cmd.ExecuteNonQuery();
                isSave = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Con.Close();
            }
            return isSave;
        }
        public static DataTable SiteOfOtherTumorTissues()
        {
            DataTable dt = new DataTable();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("SELECT  [ID],[Name] FROM T59_SiteOfOtherTumorTissues", Conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return dt;
        }

        public static List<FormFT_BO> getFormFtDetailsbyFormFtid(int Id)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            string strQuery = "";
            try
            {
                strQuery = " select T10.ID,T10.Form_FT_ID, T10.T9_PatientID,T8.SiteID,T8.Site_Name, T10.LiverTumorIssue,";
                strQuery += "  T10.DateBiospecimenCollected,T10.SubMergedIn10PercentFormalinVial,T59.ID as OtherTumorIssueId ,T59.Name as OtherTumorIssueName,T10.OtherIssue,";
                strQuery += " case when SubMergedIn10PercentFormalinVial = 1 then 'Yes' when SubMergedIn10PercentFormalinVial = 0 then 'No' else '' end as SubMergedIn10PercentFormalinVial,";
                strQuery += " T10.Created_By,T10.Created_On, T10.Modified_By,T10.Modified_On,T10.shippingCompanyId, T50.Company_name as ShippingCompanyName ,T10.trackingNumber,  ";
                strQuery += "  T10.dateShippedToDivisionOfPathology,T1A.FName+''+T1A.LName as DMG_Receieved_By, T10.DMG_Receieved_Date, T10.Record_Status,";
                strQuery += " FormFT_PersonCompletingForm_FName,FormFT_PersonCompletingForm_LName ,";
                strQuery += "  FormFT_PersonCompletingForm_Phone, FormFT_PersonCompletingForm_Email";
                strQuery += "  from T10_FormFT T10";
                strQuery += "  left outer join T59_SiteOfOtherTumorTissues T59 on(T59.ID= T10.SiteOfOtherTumorIssue and T59.Reord_Status= 'Active')";
                strQuery += " left join T50_Shipping_Company t50 on T50.ID=T10.shippingCompanyId";
                strQuery += "  left join T9_Patients T9 on (T9.Patient_ID= T10.T9_PatientID and T9.Record_Status= 'Active')";
                strQuery += " left join T8_Sites T8 on(T9.Institution= T8.SiteID and T8.Record_Status= 'Active')";
                strQuery += "  left join T1_User_Main T1 on(t1.User_ID = t10.Created_By and t1.Record_Status = 'Active')";
                strQuery += "  left join T1_User_Main T1A on(t1A.User_ID = t10.DMG_Receieved_By and t1A.Record_Status = 'Active')";
                strQuery += "  where T10.Record_Status='Active' ";
                if (Id != 0)
                {
                    strQuery += " and T10.Id=" + Id + " ";
                }

                Conn = new SqlConnection(Co);

                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormFT_BO bo = new FormFT_BO();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Form_FT_ID = dr["Form_FT_ID"].ToString();
                    bo.T9_PatientID = dr["T9_PatientID"].ToString();
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    if (dr["LiverTumorIssue"] != DBNull.Value)
                        bo.LiverTumorIssue = Convert.ToBoolean(dr["LiverTumorIssue"]);
                    if (dr["OtherIssue"] != DBNull.Value)
                        bo.OtherTIssue = Convert.ToBoolean(dr["OtherIssue"]);
                    if (dr["OtherTumorIssueId"] != DBNull.Value)
                    {
                        bo.SiteOfOtherTumorTIssue = Convert.ToInt32(dr["OtherTumorIssueId"]);
                    }
                    if (dr["OtherTumorIssueName"] != DBNull.Value)
                        bo.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                    else
                        bo.SiteOfOtherTumorTIssue = null;
                    if (dr["DateBiospecimenCollected"] != DBNull.Value)
                        bo.DateBiospecimenCollected = (DateTime)dr["DateBiospecimenCollected"];
                    if (dr["SubMergedIn10PercentFormalinVial"] != DBNull.Value)
                        bo.SubMergedIn10PercentFormalinVial = dr["SubMergedIn10PercentFormalinVial"].ToString();
                    if (dr["shippingCompanyId"] != DBNull.Value)
                    {
                        bo.shippingCompanyId = Convert.ToInt16(dr["shippingCompanyId"]);
                        bo.shippingCompanyName = dr["ShippingCompanyName"].ToString();
                        bo.dateShippedToDivisionOfPathology = (DateTime)dr["dateShippedToDivisionOfPathology"];
                        bo.trackingNumber = dr["trackingNumber"].ToString();
                    }
                    else
                    {
                        bo.shippingCompanyId = null;
                        bo.shippingCompanyName = null;
                        bo.dateShippedToDivisionOfPathology = null;
                        bo.trackingNumber = null;
                    }
                    if (dr["DMG_Receieved_By"] != DBNull.Value)
                        bo.DMG_Receieved_By = dr["DMG_Receieved_By"].ToString();
                    if (dr["DMG_Receieved_Date"] != DBNull.Value)
                        bo.DMG_Receieved_Date = (DateTime)dr["DMG_Receieved_Date"];
                    else
                        bo.DMG_Receieved_Date = null;
                    if (dr["Record_Status"] != DBNull.Value)
                        bo.Record_Status = dr["Record_Status"].ToString();
                    if (dr["FormFT_PersonCompletingForm_FName"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_FName = dr["FormFT_PersonCompletingForm_FName"].ToString();
                    if (dr["FormFT_PersonCompletingForm_LName"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_LName = dr["FormFT_PersonCompletingForm_LName"].ToString();
                    if (dr["FormFT_PersonCompletingForm_Phone"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_Phone = dr["FormFT_PersonCompletingForm_Phone"].ToString();
                    if (dr["FormFT_PersonCompletingForm_Email"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_Email = dr["FormFT_PersonCompletingForm_Email"].ToString();
                    listFormFt.Add(bo);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }

            return listFormFt;
        }
        public static List<FormFT_BO> getFormFTDetailsByUserID(string department, string role)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            DataTable dtFormFTDetails = new DataTable();
            string strQuery = "";

            try
            {
                strQuery = " select T10.ID,T10.Form_FT_ID, T10.T9_PatientID,T8.SiteID,T8.Site_Name, T10.LiverTumorIssue,";
                //strQuery +="case when LiverTumorIssue = 1 then 'true' when LiverTumorIssue = 0 then 'No' else '' end as LiverTumorIssue, ";
                //strQuery += " case when OtherIssue = 1 then 'Yes' when OtherIssue = 0 then 'No' else '' end as OtherIssue,T59.Name as OtherTumorIssueName,";// T10.DateBiospecimenCollected,";
                strQuery += "  T10.DateBiospecimenCollected,T10.SubMergedIn10PercentFormalinVial,T59.ID as OtherTumorIssueId ,T59.Name as OtherTumorIssueName,T10.OtherIssue,";
                strQuery += " case when SubMergedIn10PercentFormalinVial = 1 then 'Yes' when SubMergedIn10PercentFormalinVial = 0 then 'No' else '' end as SubMergedIn10PercentFormalinVial,";
                strQuery += " T1.FName+''+T1.LName as Created_By,T10.Created_On, T10.Modified_By,T10.Modified_On,T10.shippingCompanyId, T50.Company_name as ShippingCompanyName ,T10.trackingNumber,T9.Lab_Number,T9.Storage_Seq_Num,  ";
                strQuery += "  T10.dateShippedToDivisionOfPathology,T1A.FName+''+T1A.LName as DMG_Receieved_By, T10.DMG_Receieved_Date, T10.Record_Status,";
                strQuery += " FormFT_PersonCompletingForm_FName+' '+FormFT_PersonCompletingForm_LName as PersonCompletingName,";
                strQuery += "  FormFT_PersonCompletingForm_Phone, FormFT_PersonCompletingForm_Email";
                strQuery += "  from T10_FormFT T10";
                strQuery += "  left outer join T59_SiteOfOtherTumorTissues T59 on(T59.ID= T10.SiteOfOtherTumorIssue and T59.Reord_Status= 'Active')";
                strQuery += " left join T50_Shipping_Company t50 on T50.ID=T10.shippingCompanyId";
                strQuery += "  left join T9_Patients T9 on (T9.Patient_ID= T10.T9_PatientID and T9.Record_Status= 'Active')";
                strQuery += " left join T8_Sites T8 on(T9.Institution= T8.SiteID and T8.Record_Status= 'Active')";
                strQuery += "  left join T1_User_Main T1 on(t1.User_ID = t10.Created_By and t1.Record_Status = 'Active')";
                strQuery += "  left join T1_User_Main T1A on(t1A.User_ID = t10.DMG_Receieved_By and t1A.Record_Status = 'Active')";
                strQuery += "  where T10.Record_Status='Active' ";

                if (role == "DMG")
                {
                    strQuery += " and ( T50.[Company_name] is Not Null or [trackingNumber] is Not Null or [dateShippedToDivisionOfPathology] is Not Null)";
                }
                else if (!string.IsNullOrEmpty(department))
                {
                    strQuery += "and T9.Institution like '" + department.Substring(0, department.IndexOf("-")) + "-%'";
                }

                Conn = new SqlConnection(Co);

                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormFT_BO bo = new FormFT_BO();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Form_FT_ID = dr["Form_FT_ID"].ToString();
                    bo.T9_PatientID = dr["T9_PatientID"].ToString();
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    if (dr["LiverTumorIssue"] != DBNull.Value)
                        bo.LiverTumorIssue = Convert.ToBoolean(dr["LiverTumorIssue"]);
                    if (dr["OtherIssue"] != DBNull.Value)
                        bo.OtherTIssue = Convert.ToBoolean(dr["OtherIssue"]);
                    if (dr["OtherTumorIssueId"] != DBNull.Value)
                    {
                        bo.SiteOfOtherTumorTIssue = Convert.ToInt32(dr["OtherTumorIssueId"]);
                    }
                    if (dr["OtherTumorIssueName"] != DBNull.Value)
                        bo.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                    else
                        bo.SiteOfOtherTumorTIssue = null;
                    if (dr["DateBiospecimenCollected"] != DBNull.Value)
                        bo.DateBiospecimenCollected = (DateTime)dr["DateBiospecimenCollected"];
                    if (dr["SubMergedIn10PercentFormalinVial"] != DBNull.Value)
                        bo.SubMergedIn10PercentFormalinVial = dr["SubMergedIn10PercentFormalinVial"].ToString();
                    if (dr["shippingCompanyId"] != DBNull.Value)
                    {
                        bo.shippingCompanyId = Convert.ToInt16(dr["shippingCompanyId"]);
                        bo.shippingCompanyName = dr["ShippingCompanyName"].ToString();
                        bo.dateShippedToDivisionOfPathology = (DateTime)dr["dateShippedToDivisionOfPathology"];
                        bo.trackingNumber = dr["trackingNumber"].ToString();
                    }
                    else
                    {
                        bo.shippingCompanyId = null;
                        bo.shippingCompanyName = null;
                        bo.dateShippedToDivisionOfPathology = null;
                        bo.trackingNumber = null;
                    }
                    if (dr["DMG_Receieved_By"] != DBNull.Value)
                        bo.DMG_Receieved_By = dr["DMG_Receieved_By"].ToString();
                    if (dr["DMG_Receieved_Date"] != DBNull.Value)
                        bo.DMG_Receieved_Date = (DateTime)dr["DMG_Receieved_Date"];
                    else
                        bo.DMG_Receieved_Date = null;
                    if (dr["Record_Status"] != DBNull.Value)
                        bo.Record_Status = dr["Record_Status"].ToString();
                    if (dr["PersonCompletingName"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_FName = dr["PersonCompletingName"].ToString();
                    if (dr["FormFT_PersonCompletingForm_Phone"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_Phone = dr["FormFT_PersonCompletingForm_Phone"].ToString();
                    if (dr["FormFT_PersonCompletingForm_Email"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_Email = dr["FormFT_PersonCompletingForm_Email"].ToString();
                    if (dr["Lab_Number"] != DBNull.Value)
                        bo.Lab_Number = dr["Lab_Number"].ToString();
                    if (dr["Storage_Seq_Num"] != DBNull.Value)
                        bo.StorageSequenceNo = dr["Storage_Seq_Num"].ToString();

                    bo.Created_By = dr["Created_By"].ToString();
                    bo.Created_On = (DateTime)dr["Created_On"];
                    listFormFt.Add(bo);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return listFormFt;

        }

        public static bool UpdateFormFt(FormFT_BO bo)
        {
            bool IsUpdate = false;
            try
            {
                string strQuery = "update T10_FormFt set LiverTumorIssue='" + bo.LiverTumorIssue + "',OtherIssue ='" + bo.OtherTIssue + "'";

                if (bo.OtherTIssue)
                {
                    strQuery += " ,SiteOfOtherTumorIssue =" + bo.SiteOfOtherTumorTIssue + ",";
                }
                else
                {
                    strQuery += " ,SiteOfOtherTumorIssue =null,";
                }
                strQuery += " DateBiospecimenCollected ='" + bo.DateBiospecimenCollected + "',SubMergedIn10PercentFormalinVial='" + bo.SubMergedIn10PercentFormalinVial + "' where Form_FT_ID='" + bo.Form_FT_ID + "'";
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                IsUpdate = true;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Conn.Close();
            }
            return IsUpdate;
        }



        public static DataTable GetSlides_Locations()
        {
            DataTable dt = new DataTable();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select Location_ID, Location_Name from[dbo].[T62_Slides_Locations] where Record_Status = 'Active'", Conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static DataTable GetSlides_Status()
        {
            DataTable dt = new DataTable();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select Status_ID, Status_Name from T61_Slides_Statuses where Record_Status = 'Active'", Conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static DataTable GetReceievedConditions()
        {
            DataTable dt = new DataTable();
            try
            {
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select ReceievedCondition_ID, ReceievedCondition_Name from T51_Receieved_Conditions where Record_Status = 'Active'", Conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static FormFT_BO GetReceivedDetailsforDMG(int id)
        {
            FormFT_BO bo = new FormFT_BO();
            try
            {

                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand("select T10.Id,T10.Form_FT_ID,T9.Lab_Number,T10.dateShippedToDivisionOfPathology,T9.Patient_ID,dbo.get_StorgeSequence_By_PatientID(t9.Patient_ID) as StorageSequenceNo from T10_FormFT T10 join T9_Patients t9 on T10.T9_PatientID=t9.Patient_ID where T10.ID=" + id + "", Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    bo.ID = Convert.ToInt16(dr["Id"]);
                    bo.Form_FT_ID = dr["Form_FT_ID"].ToString();
                    bo.Lab_Number = dr["Lab_Number"].ToString();
                    bo.dateShippedToDivisionOfPathology = (DateTime)dr["dateShippedToDivisionOfPathology"];
                    bo.StorageSequenceNo = dr["StorageSequenceNo"].ToString();
                    bo.T9_PatientID = dr["Patient_ID"].ToString();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return bo;
        }
        public static bool saveReceivedDetails(FormFT_BO bo)
        {
            bool isSave = false;
            try
            {
                Conn = new SqlConnection(Co);

                string strQuery = "update T10_FormFT set DMG_Receieved_By =" + bo.DMG_Receieved_By + ", DMG_Receieved_Date = getdate() where Form_FT_ID = '" + bo.Form_FT_ID + "' and Record_Status = 'Active'";
                strQuery += ";Update T9_Patients set Storage_Seq_Num = '" + bo.StorageSequenceNo + "', Modified_By = '1', MODIFIED_ON = getdate() where Patient_ID = '" + bo.T9_PatientID + "'";
                strQuery += ";INSERT INTO [T31_FormFT_Cores_PhaseII]([Core_ID],[T10_FormFT_ID],[T64_Core_Type_ID],[T63_Receieved_Condition_Id],[T61_Status_ID],[T62_Location_ID], Received_Note)values('1','" + bo.Form_FT_ID + "','1'," + bo.Received_Condition + "," + bo.Slides_Status_ID + "," + bo.SlidesLocationID + ",'" + bo.ReceivedNote + "')";
                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                cmd.ExecuteNonQuery();
                isSave = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return isSave;
        }

        public static List<FormFT_BO> getFormFTDetailsByUserID(Protocol_role_site boprs)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            DataTable dtFormFTDetails = new DataTable();
            string strQuery = "";

            try
            {
                strQuery = " select distinct T10.ID,T10.Form_FT_ID, T10.T9_PatientID,T8.SiteID,T8.Site_Name, T10.LiverTumorIssue,";
                //strQuery +="case when LiverTumorIssue = 1 then 'true' when LiverTumorIssue = 0 then 'No' else '' end as LiverTumorIssue, ";
                //strQuery += " case when OtherIssue = 1 then 'Yes' when OtherIssue = 0 then 'No' else '' end as OtherIssue,T59.Name as OtherTumorIssueName,";// T10.DateBiospecimenCollected,";
                strQuery += "  T10.DateBiospecimenCollected,T10.SubMergedIn10PercentFormalinVial,T59.ID as OtherTumorIssueId ,T59.Name as OtherTumorIssueName,T10.OtherIssue,";
                strQuery += " case when SubMergedIn10PercentFormalinVial = 1 then 'Yes' when SubMergedIn10PercentFormalinVial = 0 then 'No' else '' end as SubMergedIn10PercentFormalinVial,";
                strQuery += " T1.FName+''+T1.LName as Created_By,T10.Created_On, T10.Modified_By,T10.Modified_On,T10.shippingCompanyId, T50.Company_name as ShippingCompanyName ,T10.trackingNumber,T9.Lab_Number,T9.Storage_Seq_Num,  ";
                strQuery += "  T10.dateShippedToDivisionOfPathology,T1A.FName+''+T1A.LName as DMG_Receieved_By, T10.DMG_Receieved_Date, T10.Record_Status,";
                strQuery += " FormFT_PersonCompletingForm_FName+' '+FormFT_PersonCompletingForm_LName as PersonCompletingName,";
                strQuery += "  FormFT_PersonCompletingForm_Phone, FormFT_PersonCompletingForm_Email";
                strQuery += "  from T10_FormFT T10";
                strQuery += "  left outer join T59_SiteOfOtherTumorTissues T59 on(T59.ID= T10.SiteOfOtherTumorIssue and T59.Reord_Status= 'Active')";
                strQuery += " left join T50_Shipping_Company t50 on T50.ID=T10.shippingCompanyId";
                strQuery += "  left join T9_Patients T9 on (T9.Patient_ID= T10.T9_PatientID and T9.Record_Status= 'Active')";
                strQuery += " left join T8_Sites T8 on(T9.Institution= T8.SiteID and T8.Record_Status= 'Active')";
                strQuery += "  left join T1_User_Main T1 on(t1.User_ID = t10.Created_By and t1.Record_Status = 'Active')";
                strQuery += "  left join T1_User_Main T1A on(t1A.User_ID = t10.DMG_Receieved_By and t1A.Record_Status = 'Active')";
                strQuery += "  left join T101_Map_Protocol_User_Role t101 on (t101.T2_Protocol_Id = t9.Protocol_Id)";
                strQuery += " left join T5_User_Roles t5 on t5.ID = t101.T5_Role_ID";

                strQuery += " left join T2_Protocol_Main t2 on t2.Id = t101.T2_Protocol_Id";
                strQuery += "  where T10.Record_Status='Active' and t9.Protocol_Id=" + boprs.ProtocolID + " ";

                if (boprs.Role_Name == "DMG")
                {
                    strQuery += " and ( T50.[Company_name] is Not Null or [trackingNumber] is Not Null or [dateShippedToDivisionOfPathology] is Not Null)";
                }
                else if (!string.IsNullOrEmpty(boprs.T8_sites_Id))
                {
                    strQuery += "and T9.Institution like '" + boprs.T8_sites_Id.Substring(0, boprs.T8_sites_Id.IndexOf("-")) + "-%'";
                }

                Conn = new SqlConnection(Co);

                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormFT_BO bo = new FormFT_BO();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Form_FT_ID = dr["Form_FT_ID"].ToString();
                    bo.T9_PatientID = dr["T9_PatientID"].ToString();
                    bo.SiteID = dr["SiteID"].ToString();
                    bo.Site_Name = dr["Site_Name"].ToString();
                    if (dr["LiverTumorIssue"] != DBNull.Value)
                        bo.LiverTumorIssue = Convert.ToBoolean(dr["LiverTumorIssue"]);
                    if (dr["OtherIssue"] != DBNull.Value)
                        bo.OtherTIssue = Convert.ToBoolean(dr["OtherIssue"]);
                    if (dr["OtherTumorIssueId"] != DBNull.Value)
                    {
                        bo.SiteOfOtherTumorTIssue = Convert.ToInt32(dr["OtherTumorIssueId"]);
                    }
                    if (dr["OtherTumorIssueName"] != DBNull.Value)
                        bo.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                    else
                        bo.SiteOfOtherTumorTIssue = null;
                    if (dr["DateBiospecimenCollected"] != DBNull.Value)
                        bo.DateBiospecimenCollected = (DateTime)dr["DateBiospecimenCollected"];
                    if (dr["SubMergedIn10PercentFormalinVial"] != DBNull.Value)
                        bo.SubMergedIn10PercentFormalinVial = dr["SubMergedIn10PercentFormalinVial"].ToString();
                    if (dr["shippingCompanyId"] != DBNull.Value)
                    {
                        bo.shippingCompanyId = Convert.ToInt16(dr["shippingCompanyId"]);
                        bo.shippingCompanyName = dr["ShippingCompanyName"].ToString();
                        bo.dateShippedToDivisionOfPathology = (DateTime)dr["dateShippedToDivisionOfPathology"];
                        bo.trackingNumber = dr["trackingNumber"].ToString();
                    }
                    else
                    {
                        bo.shippingCompanyId = null;
                        bo.shippingCompanyName = null;
                        bo.dateShippedToDivisionOfPathology = null;
                        bo.trackingNumber = null;
                    }
                    if (dr["DMG_Receieved_By"] != DBNull.Value)
                        bo.DMG_Receieved_By = dr["DMG_Receieved_By"].ToString();
                    if (dr["DMG_Receieved_Date"] != DBNull.Value)
                        bo.DMG_Receieved_Date = (DateTime)dr["DMG_Receieved_Date"];
                    else
                        bo.DMG_Receieved_Date = null;
                    if (dr["Record_Status"] != DBNull.Value)
                        bo.Record_Status = dr["Record_Status"].ToString();
                    if (dr["PersonCompletingName"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_FName = dr["PersonCompletingName"].ToString();
                    if (dr["FormFT_PersonCompletingForm_Phone"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_Phone = dr["FormFT_PersonCompletingForm_Phone"].ToString();
                    if (dr["FormFT_PersonCompletingForm_Email"] != DBNull.Value)
                        bo.FormFT_PersonCompletingForm_Email = dr["FormFT_PersonCompletingForm_Email"].ToString();
                    if (dr["Lab_Number"] != DBNull.Value)
                        bo.Lab_Number = dr["Lab_Number"].ToString();
                    if (dr["Storage_Seq_Num"] != DBNull.Value)
                        bo.StorageSequenceNo = dr["Storage_Seq_Num"].ToString();

                    bo.Created_By = dr["Created_By"].ToString();
                    bo.Created_On = (DateTime)dr["Created_On"];
                    listFormFt.Add(bo);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return listFormFt;

        }

        //
        public static bool SaveFormFTDetails_TimePoint(FormFT_BO bo, int tp_Id)
        {
            string strQuery = "";
            bool isSave = false;
            int identity = 0;
            SqlCommand cmd;
            try
            {
                Conn = new SqlConnection(Co);
                identity = FormFT_ExistedID(bo.Form_FT_ID);
                Conn.Open();
                if (identity == 0)
                {
                    strQuery = "insert into T10_FormFT(Form_FT_ID,T9_PatientID,FormFT_PersonCompletingForm_FName,FormFT_PersonCompletingForm_LName,FormFT_PersonCompletingForm_Phone,FormFT_PersonCompletingForm_Email,[Created_By],[Created_On],Record_Status)";
                    strQuery += "values('" + bo.Form_FT_ID + "','" + bo.T9_PatientID + "',";
                    // '" + bo.LiverTumorIssue + "','" + bo.OtherTIssue + "','" + bo.SubMergedIn10PercentFormalinVial + "','" + bo.DateBiospecimenCollected + "',";
                    //if (bo.OtherTIssue)
                    //{
                    //    strQuery += "'" + bo.SiteOfOtherTumorTIssue + "',";
                    //}
                    //else
                    //{
                    //    strQuery += "null,";
                    //}
                    strQuery += "'" + bo.FormFT_PersonCompletingForm_FName + "','" + bo.FormFT_PersonCompletingForm_LName + "','" + bo.FormFT_PersonCompletingForm_Phone + "','" + bo.FormFT_PersonCompletingForm_Email + "','" + bo.Created_By + "',getdate(),'Active');select SCOPE_IDENTITY()";
                    cmd = new SqlCommand(strQuery, Conn);


                    identity = Convert.ToInt16(cmd.ExecuteScalar());//identityof T10_FormFt

                }
                foreach (var item in bo.lstFt_timepoint)
                {
                    strQuery = " insert into [T10_FT_Timepoint](T102_Map_Id,T10_FTId,T10_Form_FT_ID, LiverTumorIssue,OtherIssue,SubMergedIn10PercentFormalinVial,DateBiospecimenCollected,SiteOfOtherTumorIssue) values";
                    strQuery += "(" + item.Timepoint_Id + "," + identity + ",'" + item.Form_FT_ID + "','" + item.LiverTumorIssue + "','" + item.OtherTIssue + "','" + item.SubMergedIn10PercentFormalinVial + "','" + item.DateBiospecimenCollected + "',";
                    if (item.OtherTIssue)
                    {
                        strQuery += "'" + item.SiteOfOtherTumorTIssue + "')";
                    }
                    else
                    {
                        strQuery += "null)";
                    }
                }//strQuery += ";update T9_Patients set FormFTCreatedDate=getdate() ,Modified_By='" + bo.Modified_By + "',Modified_On=getdate() where [Patient_ID]='" + bo.T9_PatientID + "'";
                cmd = new SqlCommand(strQuery, Conn);

                cmd.ExecuteNonQuery();

                isSave = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return isSave;
        }
        //Get Remaining Timepoints

        public static List<FormFT_Timepoint> GetRemaining_TimePoints_Form_Protocol(int _protocolId, int _formId, List<int> Timepointid)
        {
            List<FormFT_Timepoint> listTimepoints = new List<FormFT_Timepoint>();
            try
            {
                Conn = new SqlConnection(Co);

                String strQuery = "select T100_MapId,T100.T7_FormID,t102.T2_ProtocolID, TimePoint,t102.Id as TimepointID from T100_Map_T2Protocol_T7Forms t100 ";
                strQuery += "join T102_Map_ProtocolForm_Timepints t102 on t100.Map_ID = t102.T100_MapId where T7_FormID = " + _formId + " and t102.T2_ProtocolID=" + _protocolId + " and t102.Id not in( " + string.Join(",", Timepointid.Select(n => n.ToString()).ToArray()) + ")";

                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    FormFT_Timepoint bo = new FormFT_Timepoint();
                    bo.TimePoint = dr["TimePoint"].ToString();
                    bo.Timepoint_Id = Convert.ToInt16(dr["TimepointID"]);
                    listTimepoints.Add(bo);
                }
                Conn.Close();

            }
            catch (Exception ex)
            {
                Conn.Close();
            }
            return listTimepoints;

        }

        public static int FormFT_ExistedID(string _FormFTID)
        {
            int identity = 0;
            try
            {
                string strQuery = "Select ID from T10_FormFT where Form_FT_ID='" + _FormFTID + "'";
                Conn = new SqlConnection(Co);
                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                identity = Convert.ToInt32(cmd.ExecuteScalar());
                Conn.Close();
            }
            catch (Exception ex)
            {
                Conn.Close();
            }
            return identity;
        }

        public static bool SaveShipmentDetails_withTimpoints(FormFT_BO bo)
        {
            bool isSave = false;
            SqlCommand cmd;
            string sqlQuery = "";
            SqlConnection Con = new SqlConnection(Co);
            try
            {
                foreach (var item in bo.lstFt_timepoint)
                {
                    sqlQuery += "insert into Shipping(T102_Timepoint_ID,shippingCompanyId,trackingNumber,dateShippedToDivisionOfPathology,Created_By,Created_On,Record_Status,Form_ID)";
                    sqlQuery += "values(" + item.Timepoint_Id + "," + bo.shippingCompanyId + ",'" + bo.trackingNumber + "','" + bo.dateShippedToDivisionOfPathology + "'," + bo.Created_By + ",getdate(),'Active','" + bo.Form_FT_ID + "');";
                }
                cmd = new SqlCommand(sqlQuery, Con);
                // cmd = new SqlCommand("update T10_FormFT set shippingCompanyId=" + bo.shippingCompanyId + ",trackingNumber='" + bo.trackingNumber + "',dateShippedToDivisionOfPathology='" + bo.dateShippedToDivisionOfPathology + "',Modified_By='" + bo.Modified_By + "',Modified_On=getdate() where Form_FT_ID='" + bo.Form_FT_ID + "' ", Con);
                Con.Open();
                cmd.ExecuteNonQuery();
                isSave = true;
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Con.Close();
            }
            return isSave;
        }
        //forpatients
        public static List<Patients> list_Patients(string SiteID, string formname,int Protocolid)
        {
            StringBuilder sb_qry = new StringBuilder();
            List<Patients> list_result = new List<Patients>();
            try
            {
                Conn = new SqlConnection(Co);
                sb_qry.Append("SELECT distinct t9.id, Patient_ID FROM [T9_Patients] t9 left join T10_FormFT t10 on (t10.T9_PatientID=t9.Patient_ID)  full outer join shipping s on s.Form_ID=t10.Form_FT_ID where t9.Record_Status = 'Active' and s.Form_ID is null ");
                sb_qry.Append("and ScreenFailure = 0 and  Institution = '" + SiteID + "' and  t9.Protocol_Id="+Protocolid+"");
                if (formname == "BLK")
                    sb_qry.Append("and FormBLKCreatedDate is null");
                if (formname == "FT")
                    sb_qry.Append(" and FormFTCreatedDate is null");


                SqlCommand cmd = new SqlCommand(sb_qry.ToString(), Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                sb_qry.Clear();
                while (dr.Read())
                {
                    Patients bo = new Patients();
                    bo.ID = Convert.ToInt16(dr["ID"]);
                    bo.Patient_ID = dr["Patient_ID"].ToString();
                    list_result.Add(bo);
                }
                Conn.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }
            return list_result;
        }

        public static List<FormFT_BO> getFormFtDetailsbyFormFtid_TP(int Id)
        {
            List<FormFT_BO> listFormFt = new List<FormFT_BO>();
            string strQuery = "";
            try
            {
                strQuery = " select distinct T10.ID,T10.Form_FT_ID, T10.T9_PatientID,T8.SiteID,T8.Site_Name,";
           strQuery += " t10ft.LiverTumorIssue,t10ft.OtherIssue,t10ft.SiteOfOtherTumorIssue,t10ft.DateBiospecimenCollected,t59.Name as OtherTumorIssueName,";
               strQuery += " case when t10ft.SubMergedIn10PercentFormalinVial = 1 then 'Yes' when t10ft.SubMergedIn10PercentFormalinVial = 0 then 'No' else '' end as SubMergedIn10PercentFormalinVial,";
                strQuery += " T10.Created_By,T10.Created_On, T10.Modified_By,T10.Modified_On,s.shippingCompanyId, T50.Company_name as ShippingCompanyName ,s.trackingNumber,  ";
                strQuery += "  s.dateShippedToDivisionOfPathology,T1A.FName+''+T1A.LName as DMG_Receieved_By, T10.DMG_Receieved_Date, T10.Record_Status,";
                strQuery += " FormFT_PersonCompletingForm_FName,FormFT_PersonCompletingForm_LName ,";
                strQuery += "  FormFT_PersonCompletingForm_Phone, FormFT_PersonCompletingForm_Email,T102_Map_Id as Timepointid,t102.TimePoint";
                strQuery += "  from T10_FormFT T10";
               // strQuery += "  left outer join T59_SiteOfOtherTumorTissues T59 on(T59.ID= T10.SiteOfOtherTumorIssue and T59.Reord_Status= 'Active')";
                strQuery += " left join T50_Shipping_Company t50 on T50.ID=T10.shippingCompanyId";
                strQuery += "  left join T9_Patients T9 on (T9.Patient_ID= T10.T9_PatientID and T9.Record_Status= 'Active')";
                strQuery += " left join T8_Sites T8 on(T9.Institution= T8.SiteID and T8.Record_Status= 'Active')";
                strQuery += "  left join T1_User_Main T1 on(t1.User_ID = t10.Created_By and t1.Record_Status = 'Active')";
                strQuery += "  left join T1_User_Main T1A on(t1A.User_ID = t10.DMG_Receieved_By and t1A.Record_Status = 'Active')";
                strQuery += "  join T10_FT_Timepoint t10ft on t10ft.T10_FTId = t10.ID";

                strQuery += "  join T102_Map_ProtocolForm_Timepints t102 on t102.id = t10ft.T102_Map_Id";
                strQuery += " left join T59_SiteOfOtherTumorTissues T59 on(T59.ID = t10ft.SiteOfOtherTumorIssue and T59.Reord_Status = 'Active')";
                strQuery += " left join shipping s on s.Form_ID = t10.Form_FT_ID";
                strQuery += "  where T10.Record_Status='Active' ";
                if (Id != 0)
                {
                    strQuery += " and T10.Id=" + Id + " ";
                }

                Conn = new SqlConnection(Co);

                SqlCommand cmd = new SqlCommand(strQuery, Conn);
                Conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                FormFT_Timepoint vt;
                FormFT_BO bo = new FormFT_BO();
                bo.lstFt_timepoint = new List<FormFT_Timepoint>();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {

                        bo.ID = Convert.ToInt16(dr["ID"]);
                        bo.Form_FT_ID = dr["Form_FT_ID"].ToString();
                        bo.T9_PatientID = dr["T9_PatientID"].ToString();
                        bo.SiteID = dr["SiteID"].ToString();
                        bo.Site_Name = dr["Site_Name"].ToString();

                        if (dr["SubMergedIn10PercentFormalinVial"] != DBNull.Value)
                            bo.SubMergedIn10PercentFormalinVial = dr["SubMergedIn10PercentFormalinVial"].ToString();
                        if (dr["shippingCompanyId"] != DBNull.Value)
                        {
                            bo.shippingCompanyId = Convert.ToInt16(dr["shippingCompanyId"]);
                            bo.shippingCompanyName = dr["ShippingCompanyName"].ToString();
                            bo.dateShippedToDivisionOfPathology = (DateTime)dr["dateShippedToDivisionOfPathology"];
                            bo.trackingNumber = dr["trackingNumber"].ToString();
                        }
                        else
                        {
                            bo.shippingCompanyId = null;
                            bo.shippingCompanyName = null;
                            bo.dateShippedToDivisionOfPathology = null;
                            bo.trackingNumber = null;
                        }
                        if (dr["DMG_Receieved_By"] != DBNull.Value)
                            bo.DMG_Receieved_By = dr["DMG_Receieved_By"].ToString();
                        if (dr["DMG_Receieved_Date"] != DBNull.Value)
                            bo.DMG_Receieved_Date = (DateTime)dr["DMG_Receieved_Date"];
                        else
                            bo.DMG_Receieved_Date = null;
                        if (dr["Record_Status"] != DBNull.Value)
                            bo.Record_Status = dr["Record_Status"].ToString();
                        if (dr["FormFT_PersonCompletingForm_FName"] != DBNull.Value)
                            bo.FormFT_PersonCompletingForm_FName = dr["FormFT_PersonCompletingForm_FName"].ToString();
                        if (dr["FormFT_PersonCompletingForm_LName"] != DBNull.Value)
                            bo.FormFT_PersonCompletingForm_LName = dr["FormFT_PersonCompletingForm_LName"].ToString();
                        if (dr["FormFT_PersonCompletingForm_Phone"] != DBNull.Value)
                            bo.FormFT_PersonCompletingForm_Phone = dr["FormFT_PersonCompletingForm_Phone"].ToString();
                        if (dr["FormFT_PersonCompletingForm_Email"] != DBNull.Value)
                            bo.FormFT_PersonCompletingForm_Email = dr["FormFT_PersonCompletingForm_Email"].ToString();
                        vt = new FormFT_Timepoint();
                        if (dr["LiverTumorIssue"] != DBNull.Value)
                            vt.LiverTumorIssue = Convert.ToBoolean(dr["LiverTumorIssue"]);
                        if (dr["OtherIssue"] != DBNull.Value)
                            vt.OtherTIssue = Convert.ToBoolean(dr["OtherIssue"]);
                        if (dr["SiteOfOtherTumorIssue"] != DBNull.Value)
                        {
                            vt.SiteOfOtherTumorTIssue = Convert.ToInt32(dr["SiteOfOtherTumorIssue"]);
                        }
                        if (dr["OtherTumorIssueName"] != DBNull.Value)
                            vt.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                        else
                            bo.SiteOfOtherTumorTIssue = null;

                        if (dr["Timepointid"] != DBNull.Value)
                            vt.Timepoint_Id = Convert.ToInt32(dr["Timepointid"]);
                        if (dr["TimePoint"] != DBNull.Value)
                            vt.TimePoint = dr["TimePoint"].ToString();

                        if (dr["OtherTumorIssueName"] != DBNull.Value)
                            vt.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                        else
                            vt.SiteOfOtherTumorTIssue = null;
                        vt.Form_FT_ID = dr["Form_FT_ID"].ToString();
                        bo.lstFt_timepoint.Add(vt);
                        break;
                    }

                    while (dr.Read())
                    {
                        vt = new FormFT_Timepoint();
                        if (dr["LiverTumorIssue"] != DBNull.Value)
                            vt.LiverTumorIssue = Convert.ToBoolean(dr["LiverTumorIssue"]);
                        if (dr["OtherIssue"] != DBNull.Value)
                            vt.OtherTIssue = Convert.ToBoolean(dr["OtherIssue"]);
                        if (dr["SiteOfOtherTumorIssue"] != DBNull.Value)
                        {
                            vt.SiteOfOtherTumorTIssue = Convert.ToInt32(dr["SiteOfOtherTumorIssue"]);
                        }
                        if (dr["OtherTumorIssueName"] != DBNull.Value)
                            vt.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                        else
                            bo.SiteOfOtherTumorTIssue = null;

                        if (dr["Timepointid"] != DBNull.Value)
                            vt.Timepoint_Id = Convert.ToInt32(dr["Timepointid"]);
                        if (dr["TimePoint"] != DBNull.Value)
                            vt.TimePoint = dr["TimePoint"].ToString();

                        if (dr["OtherTumorIssueName"] != DBNull.Value)
                            vt.SiteOfOtherTumorTIssueName = dr["OtherTumorIssueName"].ToString();
                        else
                            vt.SiteOfOtherTumorTIssue = null;
                        vt.Form_FT_ID = dr["Form_FT_ID"].ToString();
                        bo.lstFt_timepoint.Add(vt);
                    }
                    listFormFt.Add(bo);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Conn.Close();
            }

            return listFormFt;
        }

        //public List<FormFT_Timepoint> GetFTTimepointdetails(string FormFTID)
        //{
        //    List<FormFT_Timepoint> list = new List<FormFT_Timepoint>();
        //    string strQuery = "";
        //    try
        //    {
        //        strQuery = "select distinct Form_FT_ID,T102_Map_Id as Timepointid,t102.TimePoint,";
        //        strQuery += " t10ft.LiverTumorIssue,t10ft.OtherIssue,t10ft.SiteOfOtherTumorIssue,t10ft.DateBiospecimenCollected,t59.Name as OtherTumorIssueName,";
        //        strQuery += " case when t10ft.SubMergedIn10PercentFormalinVial = 1 then 'Yes' when t10ft.SubMergedIn10PercentFormalinVial = 0 then 'No' else '' end as SubMergedIn10PercentFormalinVial,";
        //        strQuery += " from T10_FT_Timepoint t10ft";

        //        strQuery += "  join T102_Map_ProtocolForm_Timepints t102 on t102.id = t10ft.T102_Map_Id";
        //        strQuery += " left join T59_SiteOfOtherTumorTissues T59 on(T59.ID = t10ft.SiteOfOtherTumorIssue and T59.Reord_Status = 'Active')";
        //        strQuery += "where Form_FT_ID='" + FormFTID + "' ";
        //        Conn = new SqlConnection(Co);

        //        SqlCommand cmd = new SqlCommand(strQuery, Conn);
        //        Conn.Open();
        //        SqlDataReader dr = cmd.ExecuteReader();
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return list;
        //}
    }
}
