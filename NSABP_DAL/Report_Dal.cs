﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using NSABP_Model_BO_;

namespace NSABP_DAL
{
    public class Report_Dal
    {
        private static string Co = ConfigurationManager.ConnectionStrings["Con"].ConnectionString;
        private static SqlConnection Conn;
        private static StringBuilder sb_qry;
        public static DataTable Reports(DateTime startdate, DateTime enddate, int ProtocolId, string Role, string site)
        {
            DataTable dt = new DataTable();
            sb_qry = new StringBuilder();
            Conn = new SqlConnection(Co);
            SqlCommand cmd = new SqlCommand();
            try
            {
                sb_qry.Append("  SELECT distinct  T2.[ID],[Form_BlockID] as [Form ID],t8.SiteID as [Site ID],t8.Site_Name as [Site Name],[T1_PatientID] as [Patient ID], ");
                sb_qry.Append(" t9.Lab_Number as [Lab Number],t9.Storage_Seq_Num as [Storage No],t1.FName+t1.LName as [Created By],T2.[Created_On] as [Created On], ");
                sb_qry.Append(" t50.Company_name as [Shipping Company],[trackingNumber] as [Tracking Number],t1a.FName + T1A.LName as [Receieved By],[DMG_Receieved_Date] as [Received On]  FROM[dbo].[T2_Form_BLK_PhaseII] T2 ");
                sb_qry.Append("   left join T50_Shipping_Company t50 on(t2.shippingCompanyID = t50.ID) left join[T1_User_Main] T1 on(t1.User_ID = t2.Created_By and t1.Record_Status = 'Active')");
                sb_qry.Append(" left join[T1_User_Main] T1A on(t1A.User_ID = t2.DMG_Receieved_By and t1A.Record_Status = 'Active') join T9_Patients t9 on(t9.Patient_ID = t2.T1_PatientID) ");
                sb_qry.Append(" join[dbo].[T8_Sites] t8 on(t8.SiteID = t9.Institution) join[dbo].[T101_Map_Protocol_User_Role] ");
                sb_qry.Append("   t101 on(t101.T2_Protocol_Id = t9.Protocol_Id) join[dbo].[T5_User_Roles]  t5 on(t5.ID = t101.T5_Role_ID)");
                sb_qry.Append(" where T2.Record_Status = 'Active'  ");
                sb_qry.Append("  and T9.Protocol_Id = " + ProtocolId + " ");
                if (Role == "SiteUser")
                    sb_qry.Append("   and t8.SiteID like '"+site.Substring(0,site.IndexOf('-'))+"-%'");
                if (startdate != DateTime.MinValue && enddate != DateTime.MinValue)
                    sb_qry.Append(" and dateadd(d,-1,datediff(d,-1,T2.[Created_On])) between '" + startdate + "' and '" + enddate + "'  ");
                if (Role == "DMG")
                    sb_qry.Append("and ( T50.[Company_name] is Not Null or [trackingNumber] is Not Null or [dateShippedToDivisionOfPathology] is Not Null) ");


                sb_qry.Append(" union SELECT distinct T2.[ID],[Form_FT_ID] as [Form ID],t8.SiteID as [Site ID],t8.Site_Name as [Site Name],[T9_PatientID] as [Patient ID], ");
                sb_qry.Append(" t9.Lab_Number as [Lab Number],t9.Storage_Seq_Num as [Storage No], ");
                sb_qry.Append(" t1.FName+t1.LName as [Created By],T2.[Created_On] as [Created On],  ");
                sb_qry.Append(" t50.Company_name as [Shipping Company],[trackingNumber] as [Tracking Number], ");
                sb_qry.Append(" t1a.FName + T1A.LName as [Receieved By],[DMG_Receieved_Date] as [Received On] ");
                sb_qry.Append(" FROM[dbo].[T10_FormFT] T2 ");
                sb_qry.Append(" left join T50_Shipping_Company t50 on(t2.shippingCompanyID = t50.ID) ");
                sb_qry.Append(" left join[T1_User_Main] T1 on(t1.User_ID = t2.Created_By and t1.Record_Status = 'Active') ");
                sb_qry.Append(" left join[T1_User_Main] T1A on(t1A.User_ID = t2.DMG_Receieved_By and t1A.Record_Status = 'Active') ");
                sb_qry.Append(" join T9_Patients t9 on(t9.Patient_ID = t2.[T9_PatientID]) ");
                sb_qry.Append(" join[dbo].[T8_Sites] t8 on(t8.SiteID = t9.Institution)");
                sb_qry.Append(" join[dbo].[T101_Map_Protocol_User_Role] ");
                sb_qry.Append(" t101 on(t101.T2_Protocol_Id = t9.Protocol_Id) ");
                sb_qry.Append(" join[dbo].[T5_User_Roles] ");
                sb_qry.Append(" t5 on(t5.ID = t101.T5_Role_ID) ");
                sb_qry.Append(" where T2.Record_Status = 'Active'  ");
                sb_qry.Append("  and T9.Protocol_Id = " + ProtocolId + " ");
                if (Role == "SiteUser")
                    sb_qry.Append("   and t8.SiteID like '" + site.Substring(0, site.IndexOf('-')) + "-%'");
                if (startdate != DateTime.MinValue && enddate != DateTime.MinValue)
                    sb_qry.Append(" and dateadd(d,-1,datediff(d,-1,T2.[Created_On])) between '" + startdate + "' and '" + enddate + "'  ");
                if (Role == "DMG")
                    sb_qry.Append("and ( T50.[Company_name] is Not Null or [trackingNumber] is Not Null or [dateShippedToDivisionOfPathology] is Not Null) ");





               cmd.Connection = Conn;
                cmd.CommandText = sb_qry.ToString();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
            }
            return dt;

        }
    }
}
