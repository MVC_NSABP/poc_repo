﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using NSABP_Model_BO_;
using NSABP_DAL;

namespace NSABP_BLL
{
    public class Reports_BLL
    {
        public static DataTable Reports(DateTime startdate, DateTime enddate,int ProtocolId,string Role,string site)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = Report_Dal.Reports(startdate, enddate, ProtocolId, Role, site);
            }
            catch (Exception)
            {

                throw;
            }
            return dt;
        }
    }
}
