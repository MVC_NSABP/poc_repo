﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using NSABP_DAL;
using NSABP_Model_BO_;
namespace NSABP_BLL
{
    public class LoginBLL
    {
        public static DataTable getuserDetails(LoginBO loginBO)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = LoginDAL.getuserDetails(loginBO);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public static DataTable getForms(int user_ID)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = LoginDAL.getForms(user_ID);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
    }
}
