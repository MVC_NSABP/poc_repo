﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSABP_DAL;
using System.Data;
using NSABP_Model_BO_;
//close test

namespace NSABP_BLL
{
    public class Base_BLL
    {
        public static List<Sites> list_Sites()
        {
            List<Sites> res = new List<Sites>();
            try
            {
                res = Base_DAL.list_Sites();
               
            }
            catch (Exception ex)
            {
            }
            return res;
        }
        public static List<Shipping_Company> list_Shipping_Company()
        {
            List<Shipping_Company> res = new List<Shipping_Company>();
            try
            {
                res = Base_DAL.list_Shipping_Company();
            }
            catch (Exception ex)
            {
            }
            return res;
        }
        public static List<Patients> list_Patients(string SiteID, string formname)
        {
            List<Patients> res = new List<Patients>();
            try
            {
                res = Base_DAL.list_Patients(SiteID, formname);
            }
            catch (Exception ex)
            {
            }
            return res;
        }
        public static List<Receieved_Conditions> list_ReceivedCondition()
        {
            List<Receieved_Conditions> res = new List<Receieved_Conditions>();
            try
            {
                res = Base_DAL.list_ReceivedCondition();
            }
            catch (Exception ex)
            {
            }
            return res;
        }

        public static List<Protocol_BO> List_Protocols(int User_ID)
        {
            List<Protocol_BO> res = new List<Protocol_BO>();
            try
            {
                res = Base_DAL.List_Protocols(User_ID);
            }
            catch (Exception ex)
            {
            }
            return res;
        }
        public static List<Forms> Listforms(int Listforms)
        {
            List<Forms> res = new List<Forms>();
            try
            {
                res = Base_DAL.Listforms(Listforms);
            }
            catch (Exception ex)
            {
            }
            return res;
        }

        public static List<string> GetTimePoints_Form_Protocol(int _protocolId, int _formId)
        {
            List<string> listTimepoints = new List<string>();
            try
            {
                listTimepoints = Base_DAL.GetTimePoints_Form_Protocol(_protocolId, _formId);
            }
            catch (Exception ex)
            {
            }
            return listTimepoints;
        }

    }
}
