﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace NSABP_Model_BO_
{
    public class FormBNK_BO
    {
        public List<Sites> List_Sites { get; set; }
        public List<Patients> list_Patients { get; set; }
        public List<Shipping_Company> list_Shipping_Company { get; set; }
        public int ID { get; set; }
        public string FormBNKID { get; set; }
        [Required]
        [Display(Name = "Patient ID")]
        public string PatientID { get; set; }

        [Display(Name = "Institution Name")]
        public string Site_Name { get; set; }
        [Display(Name = "Institution Number")]
        public string SiteID { get; set; }


        public int PersonCompletingFormID { get; set; }
        //[Required]
        [Display(Name = "Last Name")]
        public string FormBNK_PersonCompletingForm_LName { get; set; }
        //[Required]
        [Display(Name = "First Name")]
        public string FormBNK_PersonCompletingForm_FName { get; set; }
        //[Required]
        [Display(Name = "Phone number")]
        public string FormBNK_PersonCompletingForm_Phone { get; set; }
        //[Required]
        [Display(Name = "Email")]
        public string FormBNK_PersonCompletingForm_Email { get; set; }
        [Required]
        [Display(Name = "Date of Cycle 1 Day 1 of neratinib and TDM1")]
        public DateTime Dt_of_D1C1_of_neratinib_and_TDM1 { get; set; }
        [Required]
        [Display(Name = "Time Neratinib dose was taken Cycle 1 Day 1")]
        public string Time_neratinib_dose_was_taken_D1C1 { get; set; }
        [Required]
        [Display(Name = "Time T-DM1 dose Cycle 1 Day 1 started")]
        public string Time_TDM1_dose_D1C1_started { get; set; }
        [Required]
        [Display(Name = "Date Sample Collected")]
        public DateTime Dt_samples_Collected_C1D1 { get; set; }
        [Required]
        [Display(Name = "Time Sample Collected")]
        public string Time_samples_collected_C1D1 { get; set; }

        [Display(Name = "Central Access Device")]
        public bool? Central_Access_Device { get; set; }
        [Display(Name = "Peripheral venous")]
        public bool? Peripheral_venous { get; set; }
        public string Peripheral_Venious_Central_Access_Deveice_C1D1 { get; set; }
        [Required]
        [Display(Name = "Time centrifugation occurred ")]
        public string Time_Centrifugation_occured_C1D1 { get; set; }
        [Required]
        [Display(Name = "Time Sample was frozen")]
        public string Time_samples_was_frozen_C1D1 { get; set; }
        [Required]
        [Display(Name = "Number of Blue top micro-tubes frozen prior to C1D1")]
        public int No_of_Blue_Top_microtubes_frozen_prior_to_C1D1 { get; set; }
        [Required]
        [Display(Name = "Number of Yellow top micro-tubes frozen prior to C1D1")]
        public int No_of_Yellow_Top_microtubes_frozen_prior_to_C1D1 { get; set; }

        public int DMG_Receieved_By { get; set; }
        public string Notes { get; set; }
        public DateTime DMG_Receieved_Date { get; set; }
        public int Received_Condition { get; set; }
        [Display(Name = "Name of the Shipping Company:")]
        public int shippingCompanyName { get; set; }
        [Display(Name = "Tracking Number:")]
        public string trackingNumber { get; set; }
        public int T50_User_ID_CREATED_BY { get; set; }
        [Display(Name = "Date Specimens shipped to Division of Pathology:")]
        public DateTime dateShippedToDivisionOfPathology { get; set; }
        public DateTime Created_On { get; set; }
        public DateTime Modified_On { get; set; }
        public int T50_User_ID_MODIFIED_BY { get; set; }
        public DateTime DateDiagnosticBreastBiopsyProcedure { get; set; }
        public bool? FormEnclosed { get; set; }
        public int numberOfSlidesReceivedByNSABP { get; set; }
        public int numberOfBlocksReceivedByNSABP { get; set; }
        public string Record_Status { get; set; }
        public string Receipt_Number { get; set; }
        public DateTime AllowSiteEditDate { get; set; }
        public string Storage_Squence_Number { get; set; }
        public string Lab_Number { get; set; }
    }
}
