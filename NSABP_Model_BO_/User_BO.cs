﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using NSABP_Model_BO_;

namespace NSABP_Model_BO_
{

    public class User_BO : Protocol_BO
    {

        public int User_ID { get; set; }
        [Display(Name = "User Name")]
        [Required]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Work Title")]
        public string Work_Title { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FName { get; set; }
        [Display(Name = "Middle Name")]
        public string MInitial { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LName { get; set; }
        [Required]
        [Display(Name = "Email")]
        public string email { get; set; }

        //public int Protocol_Id { get; set; }
        //[Display(Name = "Protocol")]
        //public string Protocol_Code { get; set; }

        //public int Role_Id { get; set; }
        public int Created_by { get; set; }
        public DateTime Created_on { get; set; }
        public string Record_Status { get; set; }
        //[Display(Name = "Role")]
        //public string Role_Name { get; set; }
        [Required]
        public int Status_Id { get; set; }
        [Display(Name = "User Status")]
        public string Status_Title { get; set; }
        
        public string PhoneNo { get; set; }
        //public IEnumerable<SelectListItem> Protocols { get; set; }   
        //public string Site { get; set; }
        public List<Sites> listSites { get; set; }
        public List<Protocol_role_site> List_Protocol_role_site { get; set; }

    }


    public class Protocol_role_site
    {
        //public int ProtocolID { get; set; }
        public int ProtocolID { get; set; }
        public int T1_User_ID { get; set; }
        public string T8_sites_Id { get; set; }
        public string Protocol_Code { get; set; }
        public int T5_Role_ID { get; set; }
        public string Role_Name { get; set; }
        

    }


}
